# This script produces a cycle plot with observed quantities

from vader import readCheckpoint
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import matplotlib.colors as colors
import matplotlib.patches as patches
import os.path as osp
import glob

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '' #'/Users/krumholz/Data/cmz/test1/'

# Parameters of runs to process
mdot = '1.0'
er = '0.050'
epsff = '0.010'

# Checkpoint to process
basename = osp.join(dirname, 'mdot'+mdot+'_er'+er+'_epsff' + \
                    epsff+'_?????.vader')
outext = '_fiducial.pdf'
files = glob.glob(basename)
files.sort()
chkname = files[-1]

# Read the checkpoint
data = readCheckpoint(chkname)

# Pointers to quantities we want
r = data.grid.r
r_h = data.grid.r_h
vphi = data.grid.vphi_g[1:-1]
beta = data.grid.beta_g[1:-1]
omega = vphi/r
kappa = np.sqrt(2*(beta+1))*omega
area = data.grid.area
t = data.tOut
col = data.colOut
pres = data.presOut
sigma = np.sqrt(pres/col)
colstar = data.userOut[:,0,:]
colwind = data.userOut[:,1,:]
pdot = data.userOut[:,2,:]
edot = data.userOut[:,3,:]
colsfr = data.userOut[:,4,:]
colwind = data.userOut[:,5,:]
colsfr_obs = data.userOut[:,6,:]
mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
mdotin[0] = mdotin[1]

# Compute scale height, virial ratio, depletion time, Q
rhostar = 2.5*vphi**2*(1+2*beta)/(4*np.pi*G*r**2)
zetad = 0.33
a = 2.0*np.pi*zetad*G*rhostar*col
b = np.pi/2.0*G*col**2
c = -pres
h = (-b + np.sqrt(b**2-4*a*c))/(2*a)
alphavir = pres/(np.pi/2.0*G*col**2*h)
tdep = col/(colsfr+1.0e-50)
rho = col/(2*h)
Q = kappa*sigma/(np.pi*G*col)

# Extract properties of the ring and the whole CMZ
idxmax = np.argmax(np.mean(colsfr, axis=0))
idx10 = np.abs(np.subtract.outer(r[idxmax], r)) < 10*pc
mgas = np.sum(col*area, axis=1)
mgas10 = np.sum(col*idx10*area, axis=1)
sfr = np.sum(colsfr*area, axis=1)
sfr10 = np.sum(colsfr*idx10*area, axis=1)
sfr_obs = np.sum(colsfr_obs*area, axis=1)
sfr10_obs = np.sum(colsfr_obs*idx10*area, axis=1)
tdep10 = mgas10/(sfr10+1e-50)
tdep_obs = mgas/(sfr_obs+1e-50)
tdep10_obs = mgas10/(sfr10_obs+1e-50)

# Get averages over the ring
hbar = np.sum(area*idx10*h, axis=1)/np.sum(area*idx10)
alphabar = np.sum(area*idx10*alphavir*col, axis=1) / \
           np.sum(area*idx10*col, axis=1)
sigmabar = np.sum(area*idx10*sigma*col, axis=1) / \
           np.sum(area*idx10*col, axis=1)
Qbar = np.sum(area*idx10*Q*col, axis=1) / \
       np.sum(area*idx10*col, axis=1)
sfrbar = np.sum(area*idx10*colsfr_obs, axis=1)
pdotbar = np.sum(area*idx10*pdot, axis=1)

# Make a cycle plot, overlaying the properties of the observed CMZ
tint = 20
skip = 2
nstart = sfrbar.size - 1 - 10*tint
plt.figure(1, figsize=(6,10))
plt.clf()
ax1 = plt.gcf().add_subplot(5,1,1)
ax2 = plt.gcf().add_subplot(5,1,2)
ax3 = plt.gcf().add_subplot(5,1,3)
ax4 = plt.gcf().add_subplot(5,1,4)
ax5 = plt.gcf().add_subplot(5,1,5)
plt.subplots_adjust(hspace=0, wspace=0, bottom=0.1, top=0.95, left=0.12, right=0.85)

# Plot H
ax1.scatter(np.log10(sfrbar[nstart::skip]/(Msun/yr)),
            hbar[nstart::skip]/pc,
            c=(t[nstart::skip]-t[nstart])/Myr, vmin=0, vmax=tint)
ax1.add_patch(patches.Ellipse((-1.2,9), 0.4, 12, #3-15 pc from Longmore+12 and Henshaw+16
                              fc='k', alpha=0.3, ec='none',
                              zorder=0))
lpatch = patches.Patch(color='k', alpha=0.3)
simArtist = plt.Line2D([0,1],[0,1], color='w',
                       marker=r'$\circlearrowleft$', ms=15, mfc='k')
ax1.legend([lpatch, simArtist], ['Observed MW ring', 'Direction of evolution'],
           loc='upper right', numpoints=1,
           prop = {'size' : 10})
ax1.set_xlim([-2,0.5])
#ax1.set_xscale('log')
ax1.set_ylim([0,20])
ax1.set_yticks([0, 5,10,15,20])
ax1.get_xaxis().set_ticks([])
ax1.set_ylabel(r'$H$ [pc]')

# Plot sigma
ax2.scatter(np.log10(sfrbar[nstart::skip]/(Msun/yr)),
            sigmabar[nstart::skip]/kmps,
            c=(t[nstart::skip]-t[nstart])/Myr, vmin=0, vmax=tint)
ax2.add_patch(patches.Ellipse((-1.2,15), 0.4, 20, #5-25 km/s from KDL15
                              fc='k', alpha=0.3, ec='none',
                              zorder=0))
ax2.set_xlim([-2,0.5])
#ax2.set_xscale('log')
ax2.set_ylim([0,24])
ax2.get_xaxis().set_ticks([])
ax2.set_yticks([0, 5,10,15,20])
ax2.set_ylabel(r'$\sigma$ [km s$^{-1}$]')

# Plot tdep
ax3.scatter(np.log10(sfrbar[nstart::skip]/(Msun/yr)),
            np.log10(tdep10_obs[nstart::skip]/Gyr),
            c=(t[nstart::skip]-t[nstart])/Myr, vmin=0, vmax=tint)
ax3.add_patch(patches.Ellipse((-1.2,0), 0.4, 1,
                              fc='k', alpha=0.3, ec='none',
                              zorder=0))
ax3.set_xlim([-2,0.5])
#ax3.set_xscale('log')
ax3.set_ylim([-2.5,1.5])
#ax3.set_yscale('log')
ax3.set_yticks([-2,-1,0,1])
ax3.get_xaxis().set_ticks([])
ax3.set_ylabel(r'$\log\,t_{\mathrm{dep}}$ [Gyr]')

# Plot mass
ax4.scatter(np.log10(sfrbar[nstart::skip]/(Msun/yr)),
            np.log10(mgas10[nstart::skip]/Msun),
            c=(t[nstart::skip]-t[nstart])/Myr, vmin=0, vmax=tint)
ax4.add_patch(patches.Ellipse((-1.2,7.26), 0.4, 0.6,
                              fc='k', alpha=0.3, ec='none',
                              zorder=0))
ax4.set_xlim([-2,0.5])
ax4.get_xaxis().set_ticks([])
ax4.set_ylim([6.9,7.6])
ax4.set_yticks([7, 7.2, 7.4, 7.6])
ax4.set_ylabel(r'$\log\, M_{\mathrm{gas}}$ [$M_\odot$]')

# Plot Toomre Q
ax5.scatter(np.log10(sfrbar[nstart::skip]/(Msun/yr)),
            Qbar[nstart::skip],
            c=(t[nstart::skip]-t[nstart])/Myr, vmin=0, vmax=tint)
ax5.add_patch(patches.Ellipse((-1.2,1.15), 0.4, 1.9, #1.05), 0.4, 1.3,
                              fc='k', alpha=0.3, ec='none',
                              zorder=0))
ax5.set_xlim([-2,0.5])
#ax5.set_xscale('log')
ax5.set_ylim([0, 4])
ax5.set_yticks([0,1,2,3,4])
ax5.set_xlabel(r'$\log\,\dot{M}_{*,\mathrm{obs}}$ [$M_\odot$ yr$^{-1}$]')
ax5.set_ylabel(r'$Q_{\mathrm{gas}}$')

# Add colorbar
cbarwidth=0.02
cbbox = ax5.get_position()
cbbox.x0 = cbbox.x1
cbbox.x1 = cbbox.x1+cbarwidth
cbbox.y1 = cbbox.y0 + 5*(cbbox.y1-cbbox.y0)
axcbar = plt.gcf().add_axes(cbbox)
norm = colors.Normalize(vmin=0, vmax=tint)
cbar = cb.ColorbarBase(axcbar, norm=norm, orientation='vertical')
cbar.set_label(r'$t$ [Myr]')

# Save
plt.savefig('obs_cycle.pdf')




