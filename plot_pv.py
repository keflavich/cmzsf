# This script produces a PV diagram

from vader import readCheckpoint
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import matplotlib.colors as colors
import os.path as osp
import glob
import scipy.misc

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '' #'/Users/krumholz/Data/cmz/test1/'

# Parameters of runs to process
mdot = '1.0'
er = '0.050'
epsff = '0.010'

# Checkpoint to process
basename = osp.join(dirname, 'mdot'+mdot+'_er'+er+'_epsff' + \
                    epsff+'_?????.vader')
outext = '_fiducial.pdf'
files = glob.glob(basename)
files.sort()
chkname = files[-1]

# Read the checkpoint
data = readCheckpoint(chkname)

# Pointers to quantities we want
r = data.grid.r
r_h = data.grid.r_h
vphi = data.grid.vphi_g[1:-1]
beta = data.grid.beta_g[1:-1]
omega = vphi/r
kappa = np.sqrt(2*(beta+1))*omega
area = data.grid.area
t = data.tOut
col = data.colOut
pres = data.presOut
sigma = np.sqrt(pres/col)
colstar = data.userOut[:,0,:]
colwind = data.userOut[:,1,:]
pdot = data.userOut[:,2,:]
edot = data.userOut[:,3,:]
colsfr = data.userOut[:,4,:]
colwind = data.userOut[:,5,:]
colsfr_obs = data.userOut[:,6,:]
mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
mdotin[0] = mdotin[1]

# Compute scale height, virial ratio, depletion time
rhostar = 2.5*vphi**2*(1+2*beta)/(4*np.pi*G*r**2)
zetad = 0.33
a = 2.0*np.pi*zetad*G*rhostar*col
b = np.pi/2.0*G*col**2
c = -pres
h = (-b + np.sqrt(b**2-4*a*c))/(2*a)
alphavir = pres/(np.pi/2.0*G*col**2*h)
tdep = col/(colsfr+1.0e-50)
rho = col/(2*h)

# Make grid for Galactic coordinates map
Rgc = 8.5e3*pc    # Assumed GC distance
lmax = 1.6
lplot = 1.5
nl = 256
vmax = 215.0
vplot = 200.0
nv = 200
lgrid = np.linspace(-lmax,lmax,nl)
vgrid = np.linspace(-vmax*kmps,vmax*kmps,nv+1)
lvgrid = np.zeros((nl,nv))+1e-50
lvgridint = np.zeros((nl,nv))+1e-50

# Position of Sgr A*, from SIMBAD; all coordinates are relative to it
lsgra = 359.9442 - 360
bsgra = -0.0462
vsgra = -14.0

# Read in KDL15 orbital model
file = 'orbit_KDL15.dat'
data = np.genfromtxt(file, usecols=range(0,16), skip_header=3)
lorb = data[:,9]
borb = data[:,10]
vlosorb = data[:,11]

# Time index to plot
tidx = 4851

# Loop through bins in l
for i, l in enumerate(lgrid):

    # Displacement of this los from galactic center
    d = Rgc*np.sin(l*2*np.pi/360.)

    # Indices of annuli this ray intersects
    idx = np.where(r_h[1:] > np.abs(d))[0]

    # Get LOS velocities of this annuli
    vlos = -vphi[idx] * d/r[idx]

    # Generate grid of velocities around the LOS velocity
    offset = np.linspace(-4,4,401)
    voffset = np.outer(offset, sigma[tidx,idx]/np.sqrt(3))
    v = vlos + voffset
    vwgt = np.exp(-offset**2/2)
    vwgt = vwgt/np.sum(vwgt)

    # Get path length of ray through annulus; handle special case of a
    # ray that only interects the annulus once
    s = 2*(np.sqrt(r_h[idx+1]**2-d**2) -
           np.sqrt(np.maximum(r_h[idx]**2-d**2, 0.0)))
    wgt = np.outer(vwgt, rho[tidx,idx]*s)
    wgtint = np.outer(vwgt, col[tidx,idx]*s)

    # Sum data in bins and add to sum
    histint, edge = np.histogram(v, bins = vgrid, weights = wgtint)
    lvgridint[i,:] += histint
    hist, edge = np.histogram(v, bins = vgrid, weights = wgt)
    lvgrid[i,:] += hist


# Convert to observer units: H / cm^2 / km s^-1 and Msun / deg / km s^-1
dN2dv = lvgrid/((vgrid[1]-vgrid[0])/kmps)/2.34e-24
dM1dv = lvgridint/((vgrid[1]-vgrid[0])/kmps)/1.99e33*Rgc/360.*2*np.pi

# Plot
plt.figure(1, figsize=(6,8))

plt.clf()
ax1=plt.subplot(2,1,1)
ax2=plt.subplot(2,1,2)
plt.subplots_adjust(bottom=0.225, top=0.825, hspace=0.125, right=0.95,
                    left=0.15)
plt.sca(ax1)
plt.imshow(np.transpose(np.log10(dN2dv)),
           origin='lower', aspect='auto',
           extent=(lmax+lsgra,-lmax+lsgra,-vmax+vsgra,vmax+vsgra),
           vmin=18, vmax=23)
plt.colorbar(ax=ax1, ticks=[18,19,20,21,22,23],
             label=r'$dN_{\mathrm{H}}/dv$ [cm$^{-2}$ (km s$^{-1}$)$^{-1}$]')
plt.plot(lorb,vlosorb, 'black')
plt.plot([lsgra], [vsgra], 'k*', label=r'Sgr A$^*$')
plt.plot([0.67], [58.2], 'bo', label='Sgr B2')
plt.plot([0.5], [46], 'ro', label='Sgr B1')
plt.plot([-0.51], [-55.5], 'go', label='Sgr C')
plt.plot([0.121], [95], 'cs', label='Arches')
plt.plot([0.160], [102], 'ys', label='Quintuplet')
#plt.text(-0.6, 150, 'Midplane', fontdict={'color' : 'w'})
plt.legend(loc='upper right', title='Midplane',
           prop={'size':8}, numpoints=1)
#plt.legend(loc='lower left',
#           prop={'size':10}, numpoints=1)
plt.xlim([lplot,-lplot])
plt.ylim([-vplot,vplot])
ax1.set_xticklabels([])
ax1.set_yticks([-200,-150,-100,-50,0,50,100,150])

plt.sca(ax2)
plt.imshow(np.transpose(np.log10(dM1dv)),
           origin='lower', aspect='auto',
           extent=(lmax+lsgra,-lmax+lsgra,-vmax+vsgra,vmax+vsgra),
           vmin=2, vmax=6)
plt.plot(lorb,vlosorb, 'black')
plt.plot([lsgra], [vsgra], 'k*', label=r'Sgr A$^*$')
plt.plot([0.67], [58.2], 'bo', label='Sgr B2')
plt.plot([0.5], [46], 'ro', label='Sgr B1')
plt.plot([-0.51], [-55.5], 'go', label='Sgr C')
plt.plot([0.121], [95], 'cs', label='Arches')
plt.plot([0.160], [102], 'ys', label='Quintuplet')
plt.xlim([lplot,-lplot])
plt.ylim([-vplot,vplot])
plt.legend([], [], loc='upper right', title='z-integrated',
           prop={'size':8})
#plt.text(-0.6, 150, r'z-integrated', fontdict={'color' : 'w'})
plt.colorbar(ax=ax2, ticks=[2,3,4,5,6],
             label=r'$d^2M/d\ell\,dv$ [$M_\odot$ deg$^{-1}$ (km s$^{-1}$)$^{-1}$]')
ax2.set_xticklabels([])
ax2.set_yticks([-150,-100,-50,0,50,100,150,200])

ax = plt.gcf().add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_visible(False)
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.set_ylabel(r'$v$ [km s$^{-1}$]', labelpad=40)

ax1abox = ax1.get_position()
dy = ax1abox.height
ax1abox.y0 = ax1abox.y1+0.05*dy
ax1abox.y1 = ax1abox.y1+0.5*dy
ax1a = plt.gcf().add_axes(ax1abox)
ax1a.plot(lgrid, np.sum(dN2dv,axis=1)*((vgrid[1]-vgrid[0])/kmps), 'k',
          lw=2)
ax1a.set_yscale('log')
ax1a.set_ylabel(r'$N_{\rm H}$ [cm$^{-2}$]')
ax1a.set_xticklabels([])
ax1a.set_xlim([lplot,-lplot])

ax2abox = ax2.get_position()
dy = ax2abox.height
ax2abox.y1 = ax2abox.y0-0.05*dy
ax2abox.y0 = ax2abox.y0-0.5*dy
ax2a = plt.gcf().add_axes(ax2abox)
ax2a.plot(lgrid, np.sum(dM1dv,axis=1)*((vgrid[1]-vgrid[0])/kmps), 'k',
          lw=2)
ax2a.set_yscale('log')
ax2a.set_ylabel(r'$dM/d\ell$ [$M_\odot$ deg$^{-1}$]')
ax2a.set_xlabel(r'$\ell$ [deg]')
ax2a.set_xlim([lplot,-lplot])

figsave = plt.gcf()
figsave.set_size_inches(6,8)
plt.savefig('pv_plot.pdf')
