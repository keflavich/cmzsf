# This script runs all the simulations for Krumholz & Kruijssen (2016)

# Import libraries
import os
import os.path as osp
import glob
import sys
import subprocess
import time
import threading

# List of parameters to vary
mdot   = [  0.1,   0.3,  1.0,   0.3, 0.3,   0.3,   0.3 ]
ecc    = [ 0.05,  0.05, 0.05, 0.025, 0.1,  0.05,  0.05 ]
epsff  = [ 0.01,  0.01, 0.01,  0.01, 0.01, 0.005, 0.02 ]
enwind = [    0,    0,    0,    0,    0,     0,    0 ] 
#mdot   = [  0.1,  0.3,  1.0,  0.3,  0.3,   0.3,  0.3 ]
#ecc    = [  0.1,  0.1,  0.1, 0.05,  0.2,   0.1,  0.1 ]
#epsff  = [ 0.01, 0.01, 0.01, 0.01, 0.01, 0.005, 0.02 ]
#enwind = [    0,    0,    0,    0,    0,     0,    0 ] 
nrun = len(mdot)

# Template parameter file
paramFile = 'template.param'

# Time for which to run, in Myr
tRun = 500.0

# Location to write / read outdir
outdir = "/Users/krumholz/Data/cmz/test1/"

# Number of simulataneous jobs to allow
nproc = 7

# Location of vader
path_to_vader = '/Users/krumholz/Projects/viscdisk/vader'

# Frequency with which to flush output buffers, in sec
flushtime = 30

################# NO USER PARAMETERS BELOW HERE ################

# Rebuild VADER c library
print("Rebuilding c library...")
os.system('cd '+path_to_vader+'; make lib PROB=cmzdisk2')
import vader

# Define a function to handle IO buffering
def transfer_output(stdout, fp):
    for line in iter(stdout.readline, ''):
        fp.write(line)
    fp.close()

# Start the clock
tstart = time.time()
ctr = 0

# Loop over runs to be done
procs = [ None ] * nproc
runs = [ 'Waiting' ] * nrun
io_threads = [ None ] * nproc
while True:

    # Has a job finished? If so, mark the run as done and remove it
    # from process list.
    for i in range(nproc):
        if procs[i]:
            if procs[i]['proc'].poll() is not None:
                runs[procs[i]['runnum']] = 'Done'
                procs[i] = None

    # Flush log files periodically
    if flushtime:
        if time.time() - tstart > flushtime*ctr:
            ctr += 1
            for i in range(nproc):
                if procs[i]:
                    procs[i]['stdout'].flush()

    # Are all runs done? If so, exit main loop.
    if 'Waiting' not in runs and 'Running' not in runs:
        break

    # See if we have a job slot free; if not, just loop
    if None not in procs:
        continue

    # Are there any jobs waiting? If not, just loop.
    if 'Waiting' not in runs:
        continue
    
    # If we're here, we can start a new process; figure out which set
    # of parameters to use
    i = runs.index('Waiting')

    # Flag that run has started
    runs[i] = 'Running'

    # Construct the output names for this run
    basename = 'mdot{:03.1f}_er{:05.3f}_epsff{:05.3f}'. \
               format(mdot[i], ecc[i], epsff[i])
    checkname = osp.join(outdir, basename)
    outname = osp.join(outdir, basename+'.txt')
                         
    # See if we already have a checkpoint for this run, and if so,
    # figure out its time
    # See if we have a checkpoint to restart from
    chkfiles = glob.glob(checkname + '_?????.vader')
    chkfiles.sort()
    if len(chkfiles) > 0:
        restart = chkfiles[-1]
    else:
        restart = None
    
    # If we have a checkpoint, check if it goes up to the last time we
    # want; if so, set this job to done and continue
    if restart is not None:
        data = vader.readCheckpoint(restart)
        if data.t == tRun:
            runs[i] = True
            continue

    # If we're here, we're going to launch a simulation, so construct
    # the command line
    cmd = sys.executable + ' runsim.py ' + paramFile + \
          ' --outdir ' + outdir + \
          ' --mdot ' + str(mdot[i]) + \
          ' --er ' + str(ecc[i]) + \
          ' --epsff0 ' + str(epsff[i]) + \
          ' --trun ' + str(tRun)
    if enwind[i]:
        cmd += ' --enwind'
    if restart:
        cmd += ' --restart '+restart

    # Launch process
    fp = open(outname, 'w')
    print cmd + ' > ' + outname
    p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE,
                         bufsize=0)

    # Record process
    idx = procs.index(None)
    procs[idx] = { 'proc' : p, 'runnum' : i, 'stdout' : fp }

    # Start thread to handle output transfer
    io_threads[idx] = threading.Thread(
        target=transfer_output, args=(p.stdout, fp))
    io_threads[idx].daemon = True
    io_threads[idx].start()
