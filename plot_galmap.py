# This script produces a column density map from the fiducial run

from vader import readCheckpoint
import numpy as np
import numpy.ma as ma
import matplotlib.pyplot as plt
import matplotlib.colorbar as cb
import matplotlib.colors as colors
import os.path as osp
import glob
import scipy.misc
import matplotlib.cm as cm
import matplotlib.colors as colors

# Units
import scipy.constants as physcons
G = physcons.G*1e3
import astropy.units as astropyu
Msun = astropyu.Msun.to('g')
pc = astropyu.pc.to('cm')
yr = astropyu.yr.to('s')
Myr = 1e6*yr
Gyr = 1e9*yr
kmps = 1e5

# Directory where data lives
dirname = '' #'/Users/krumholz/Data/cmz/test1/'

# Parameters of runs to process
mdot = '1.0'
er = '0.050'
epsff = '0.010'

# Checkpoint to process
basename = osp.join(dirname, 'mdot'+mdot+'_er'+er+'_epsff' + \
                    epsff+'_?????.vader')
outext = '_fiducial.pdf'
files = glob.glob(basename)
files.sort()
chkname = files[-1]

# Read the checkpoint
data = readCheckpoint(chkname)

# Pointers to quantities we want
r = data.grid.r
r_h = data.grid.r_h
vphi = data.grid.vphi_g[1:-1]
beta = data.grid.beta_g[1:-1]
omega = vphi/r
kappa = np.sqrt(2*(beta+1))*omega
area = data.grid.area
t = data.tOut
col = data.colOut
pres = data.presOut
sigma = np.sqrt(pres/col)
colstar = data.userOut[:,0,:]
colwind = data.userOut[:,1,:]
pdot = data.userOut[:,2,:]
edot = data.userOut[:,3,:]
colsfr = data.userOut[:,4,:]
colwind = data.userOut[:,5,:]
colsfr_obs = data.userOut[:,6,:]
mdotin = -data.mBndOut[:,1]/(t+1.0e-50)
mdotin[0] = mdotin[1]

# Compute scale height, virial ratio, depletion time
rhostar = 2.5*vphi**2*(1+2*beta)/(4*np.pi*G*r**2)
zetad = 0.33
a = 2.0*np.pi*zetad*G*rhostar*col
b = np.pi/2.0*G*col**2
c = -pres
h = (-b + np.sqrt(b**2-4*a*c))/(2*a)
alphavir = pres/(np.pi/2.0*G*col**2*h)
tdep = col/(colsfr+1.0e-50)

# Make grid for Galactic coordinates map
Rgc = 8.5e3*pc    # Assumed GC distance
lmax = 1.6
bmax = 0.8
nb = 64
nl = int(lmax/bmax*nb)
lgrid=np.linspace(0,lmax,nl)*2.0*np.pi/360.
bgrid=np.linspace(0,bmax,nb)*2.0*np.pi/360.
dl = (lgrid[1]-lgrid[0])*180/np.pi
db = (bgrid[1]-bgrid[0])*180/np.pi

# Position of Sgr A*, from SIMBAD; all coordinates are relative to it
lsgra = 359.9442 - 360
bsgra = -0.0462

# Structure to hold output
outint = 10000
minidx = 4851
galcol = np.zeros((col[minidx::outint,:].shape[0],nl,nb))

# Loop in r and l
for i, l in enumerate(lgrid):

    # Useful stuff
    sinl = np.sin(l)
    cosl = np.cos(l)

    for j, b in enumerate(bgrid):

        # More useful stuff
        secb = 1.0/np.cos(b)

        # Get normalized r
        rnorm = r/Rgc
        rhnorm = r_h/Rgc

        # Get z+ and z-
        zplus = Rgc*np.tan(b)*(cosl+np.sqrt(
            np.maximum(rnorm**2-sinl**2, 0)))
        zminus = Rgc*np.tan(b)*(cosl-np.sqrt(
            np.maximum(rnorm**2-sinl**2, 0)))

        # Get rho
        rhoplus = col[minidx::outint]/(2.0*h[minidx::outint]) / \
                  np.cosh(zplus/h[minidx::outint])**2
        rhominus = col[minidx::outint]/(2.0*h[minidx::outint]) / \
                   np.cosh(zminus/h[minidx::outint])**2

        # Find cells that the ray goes through, and add their
        # contribution
        idx = np.where(rhnorm[:-1] > sinl)[0]
        galcol[:,i,j] = Rgc*np.sum(
            (rhominus[:,idx]+rhoplus[:,idx])*secb*
            (np.sqrt(rhnorm[idx+1]**2-sinl**2) -
             np.sqrt(rhnorm[idx]**2-sinl**2)), axis=1)

        # Add contribution for cells that the ray is tangent to
        if idx[0] > 0:
            galcol[:,i,j] \
                = galcol[:,i,j] + Rgc * \
                (rhominus[:,idx[0]-1]+rhoplus[:,idx[0]-1]) * secb * \
                            np.sqrt(rhnorm[idx[0]]**2-sinl**2)

# Duplicate image
galimg = np.zeros((col[minidx::outint,:].shape[0],2*nl,2*nb))
galimg[:,:nl,:nb] = galcol[:,::-1,::-1]
galimg[:,nl:,:nb] = galcol[:,:,::-1]
galimg[:,:nl,nb:] = galcol[:,::-1,:]
galimg[:,nl:,nb:] = galcol

# Make number density image
NH = galimg/2.34e-24

# Read Molinari+ 2011 image
NH_obs_raw = scipy.misc.imread('molinari_map.jpg')

# Set coordinate system for observed image
dl_obs = 0.2/(621-486)
db_obs = 0.1/(387-319.5)
lmin_obs = -dl_obs*(NH_obs_raw.shape[1]-621)
lmax_obs = dl_obs*621
bmin_obs = -db_obs*(NH_obs_raw.shape[0]-319.5)
bmax_obs = db_obs*319.5

# Resize the observed image to have pixels the same size as the
# simulation map; then embed it in an array of the same size as the
# simulation map
NH_obs_scale = scipy.misc.imresize(np.swapaxes(NH_obs_raw,0,1),
                                   size=dl_obs/dl)
NH_obs = np.zeros(NH.shape[1:]+(4,), dtype='uint8')
loffset = int(round((lmax+lsgra - lmax_obs)/dl))
boffset = int(round((bmax+bsgra - bmax_obs)/db))
NH_obs[loffset:loffset+NH_obs_scale.shape[0],
       boffset:boffset+NH_obs_scale.shape[1], :-1] \
       = NH_obs_scale

# Set alpha channel value
NH_obs[:,:,3] = 255*(NH_obs[:,:,0] > 0)

# Make plot of model; note that we displace the image slightly to
# place Sgr A* at its true coordinates rather than exactly (0,0)
lplot=1.5
bplot=0.75
plt.figure(1, figsize=(6.5,5.25))
plt.clf()
plt.subplot(2,1,1)
plt.subplots_adjust(bottom=0.1, right=0.95, left=0.12, wspace=0, hspace=0)
plt.imshow(np.transpose(np.log10(NH[-1,:,:])),
           origin='lower',
           extent=(lmax+lsgra,-lmax+lsgra,
                   -bmax+bsgra,bmax+bsgra), aspect='equal',
           vmin=21, vmax=25)
plt.xlim([lplot,-lplot])
plt.ylim([-bplot,bplot])
cbar=plt.colorbar(label=r'$N_{\mathrm{H}}$ [cm$^{-2}$]')
cbar.set_ticks([21,22,23,24,25])
plt.xticks([])

# Make plot of model superimposed on observation
plt.subplot(2,1,2)
plt.imshow(np.swapaxes(NH_obs,0,1),
           extent=(lmax+lsgra,-lmax+lsgra,-bmax+bsgra,bmax+bsgra),
           )#aspect='equal')
norm = colors.Normalize(vmin=np.log10(4e22), vmax=np.log10(4e25))
img = cm.get_cmap('hot')(norm(np.transpose(np.log10(NH[-1,:,:]))))
img[:,:,3] = img[:,:,3]*np.transpose(NH[-1,:,:] > 4e22)*0.5
plt.imshow(img, origin='lower',
           extent=(lmax+lsgra,-lmax+lsgra,
                   -bmax+bsgra,bmax+bsgra), aspect='equal')

# Plot various points of interest
plt.plot([lsgra], [bsgra], 'k*', label=r'Sgr A$^*$')
plt.plot([0.67], [-0.04], 'bo', label='Sgr B2')
plt.plot([0.51], [-0.055], 'ro', label='Sgr B1')
plt.plot([-0.51], [-0.14], 'go', label='Sgr C')
plt.plot([0.121], [0.017], 'cs', label='Arches')
plt.plot([0.160], [-0.059], 'ys', label='Quintuplet')
plt.legend(loc='upper center', numpoints=1, prop={'size':10}, ncol=3)
plt.xlabel(r'$\ell$ [deg]')
plt.xlim([lplot,-lplot])
plt.ylim([-bplot,bplot])

# Add colorbar
cax, kw = cb.make_axes(plt.gca())
cbar = cb.ColorbarBase(cax, cmap='hot', norm=norm,
                       ticks=[23,24,25],
                       label=r'$N_{\mathrm{H}}$ [cm$^{-2}$]')

# Add b label on left
ax = plt.gcf().add_subplot(1,1,1)
ax.spines['top'].set_color('none')
ax.spines['bottom'].set_color('none')
ax.spines['left'].set_color('none')
ax.spines['right'].set_color('none')
ax.tick_params(top='off', bottom='off', left='off', right='off')
ax.patch.set_visible(False)
ax.get_xaxis().set_ticks([])
ax.get_yaxis().set_ticks([])
ax.set_ylabel(r'$b$ [deg]', labelpad=35)


# Save figure
figsave = plt.gcf()
figsave.set_size_inches(6.5,5.25)
plt.savefig('galmap.pdf')
